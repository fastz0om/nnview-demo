﻿using System;
using System.Collections.Generic;
using System.Text;
using Keras.Layers;
using Keras.Models;

namespace NNmodule.models
{

    class AutoencoderNeuroNet : NeuroNet
    {
        public AutoencoderNeuroNet(string name, string description, List<Dictionary<string, object>> layersDescription, 
            List<BaseLayer> layers = null, bool isTrained = false, Sequential model = null)
            : base(name, description, layersDescription, layers, isTrained, model)
        {
        }

        public List<double> Teach(Teacher teacher, int epochQuantity, bool initWeights, List<List<Image>> trainingSamples, List<List<Image>> validateSamples)
        {
            throw new NotImplementedException();
        }
    }
}
