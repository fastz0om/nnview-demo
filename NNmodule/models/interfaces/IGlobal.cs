﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NNmodule.models
{
    public struct Image
    {
        public String[] _sources;
        public List<List<double>> _functions;
        public String _content;
        public String _name;
    }

    public struct FeaturesVector
    {
        public List<double> featuresValues;
        public List<ushort> featuresIds;
        public List<int> partsCounters;
        public List<String> partsNames;
    }

    public interface IImageToRealizationConverter
    {
        FeaturesVector GetFeaturesValues(Image img, ushort firstFeatureId, FeaturesVector featuresVector,
            double[] parameters);
        bool ImageFunctionsMustBeNormalized();
    }
}
