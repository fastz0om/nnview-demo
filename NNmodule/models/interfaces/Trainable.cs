﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NNmodule.models
{
    public struct Teacher
    {
        public string optimizer;
        public string loss;
        public string[] metrics;
        public float[] loss_weights;
        public string sample_weight_mode;
        public string[] weighted_metrics;
        public Teacher(string optimizer, string loss, string[] metrics = null,
            float[] loss_weights = null, string sample_weight_mode = "None", string[] weighted_metrics = null)
        {
            this.optimizer = optimizer;
            this.loss = loss;
            this.metrics = metrics;
            this.loss_weights = loss_weights;
            this.sample_weight_mode = sample_weight_mode;
            this.weighted_metrics = weighted_metrics;
        }
    }
  
    interface ITrainable
    {
        List<double> Teach(Teacher teacher, int epochQuantity, bool initWeights, List<List<Image>> trainingSamples,
           List<List<Image>> validateSamples);
    }
}
