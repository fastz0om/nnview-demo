﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NNmodule.models
{
    interface IClassifier
    {
        public Keras.Callbacks.History Teach(Teacher teacher, int epochQuantity, bool initWeights,
            List<List<Image>> trainingSamples, List<List<Image>> validateSamples);

    }
}
