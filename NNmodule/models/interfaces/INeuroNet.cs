﻿using System;
using System.Collections.Generic;
using System.Text;
using Keras.Layers;
using Keras.Models;

namespace NNmodule.models
{
    interface INeuroNet
    {
        Sequential Model { get; set; }
        String Name { get; set; }
        String Description { get; set; }
        bool IsTrained { get; set; }
        List<BaseLayer> Layers { get; set; }
        List<Dictionary<String, Object>> LayersDescription { get; set; }
        int GetLayersNumber();
        String GetLayerType(int layerIndex);
        int GetNeuronsNumber();
        String GetActivationType(int layerIndex);
        void AddLayer(Dictionary<String, Object> layer);
        void DeleteLayer(int layerIndex);
        void Compile();
        void Save(String path);




    }
}
