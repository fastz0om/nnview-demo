﻿using System;
using System.Collections.Generic;
using System.Text;
using Keras;

namespace NNmodule.models
{
    public struct FulConDescription
    {
        public Dictionary<String, Object> Parameters;
        public FulConDescription(int units, int? input_dim = null, string activation = "", bool use_bias = true,
            string kernel_initializer = "glorot_uniform", string bias_initializer = "zeros", string kernel_regularizer = "",
            string bias_regularizer = "", string activity_regularizer = "", string kernel_constraint = "", string bias_constraint = "",
                    Shape input_shape = null)
        {
            Parameters = new Dictionary<string, object>();
            Parameters["type"] = "FulCon";
            Parameters["units"] = units;
            Parameters["input_dim"] = input_dim;
            Parameters["activation"] = activation;
            Parameters["use_bias"] = use_bias;
            Parameters["kernel_initializer"] = kernel_initializer;
            Parameters["bias_initializer"] = bias_initializer;
            Parameters["kernel_regularizer"] = kernel_regularizer;
            Parameters["bias_regularizer"] = bias_regularizer;
            Parameters["activity_regularizer"] = activity_regularizer;
            Parameters["kernel_constraint"] = kernel_constraint;
            Parameters["bias_constraint"] = bias_constraint;
            Parameters["input_shape"] = input_shape;
        }

    }
    public struct Conv1DDescription
    {
        public Dictionary<String, Object> Parameters;
        public Conv1DDescription(int filters, int kernel_size, int strides = 1, string padding = "valid", string data_format = "channels_last",
                    int dilation_rate = 1, string activation = "", bool use_bias = true, string kernel_initializer = "glorot_uniform",
                    string bias_initializer = "zeros", string kernel_regularizer = "", string bias_regularizer = "",
                    string activity_regularizer = "", string kernel_constraint = "", string bias_constraint = "",
                    Tuple<int, int, int> input_shape = null)
        {
            Parameters = new Dictionary<string, object>();
            Parameters["type"] = "Conv1D";
            Parameters["filters"] = filters;
            Parameters["kernel_size"] = kernel_size;
            Parameters["strides"] = strides;
            Parameters["padding"] = padding;
            Parameters["data_format"] = data_format;
            Parameters["dilation_rate"] = dilation_rate;
            Parameters["activation"] = activation;
            Parameters["use_bias"] = use_bias;
            Parameters["kernel_initializer"] = kernel_initializer;
            Parameters["bias_initializer"] = bias_initializer;
            Parameters["kernel_regularizer"] = kernel_regularizer;
            Parameters["bias_regularizer"] = bias_regularizer;
            Parameters["activity_regularizer"] = activity_regularizer;
            Parameters["kernel_constraint"] = kernel_constraint;
            Parameters["bias_constraint"] = bias_constraint;
            Parameters["input_shape"] = input_shape;

        }

    }

    public struct Conv2DDescription
    {
        public Dictionary<String, Object> Parameters;
        public Conv2DDescription(int filters, Tuple<int, int> kernel_size, Tuple<int, int> strides = null, string padding = "valid", string data_format = "channels_last",
                    Tuple<int, int> dilation_rate = null, string activation = "", bool use_bias = true, string kernel_initializer = "glorot_uniform",
                    string bias_initializer = "zeros", string kernel_regularizer = "", string bias_regularizer = "",
                    string activity_regularizer = "", string kernel_constraint = "", string bias_constraint = "",
                    Tuple<int, int, int, int> input_shape = null)
        {
            Parameters = new Dictionary<string, object>();
            Parameters["type"] = "Conv2D";
            Parameters["filters"] = filters;
            Parameters["kernel_size"] = kernel_size;
            Parameters["strides"] = strides;
            Parameters["padding"] = padding;
            Parameters["data_format"] = data_format;
            Parameters["dilation_rate"] = dilation_rate;
            Parameters["activation"] = activation;
            Parameters["use_bias"] = use_bias;
            Parameters["kernel_initializer"] = kernel_initializer;
            Parameters["bias_initializer"] = bias_initializer;
            Parameters["kernel_regularizer"] = kernel_regularizer;
            Parameters["bias_regularizer"] = bias_regularizer;
            Parameters["activity_regularizer"] = activity_regularizer;
            Parameters["kernel_constraint"] = kernel_constraint;
            Parameters["bias_constraint"] = bias_constraint;
            Parameters["input_shape"] = input_shape;
        }

    }

    public struct Conv3DDescription
    {
        public Dictionary<String, Object> Parameters;
        public Conv3DDescription(int filters, Tuple<int, int, int> kernel_size, Tuple<int, int, int> strides = null, string padding = "valid", string data_format = "channels_last",
                    Tuple<int, int, int> dilation_rate = null, string activation = "", bool use_bias = true, string kernel_initializer = "glorot_uniform",
                    string bias_initializer = "zeros", string kernel_regularizer = "", string bias_regularizer = "",
                    string activity_regularizer = "", string kernel_constraint = "", string bias_constraint = "")
        {
            Parameters = new Dictionary<string, object>();
            Parameters["type"] = "Conv3D";
            Parameters["filters"] = filters;
            Parameters["kernel_size"] = kernel_size;
            Parameters["strides"] = strides;
            Parameters["padding"] = padding;
            Parameters["data_format"] = data_format;
            Parameters["dilation_rate"] = dilation_rate;
            Parameters["activation"] = activation;
            Parameters["use_bias"] = use_bias;
            Parameters["kernel_initializer"] = kernel_initializer;
            Parameters["bias_initializer"] = bias_initializer;
            Parameters["kernel_regularizer"] = kernel_regularizer;
            Parameters["bias_regularizer"] = bias_regularizer;
            Parameters["activity_regularizer"] = activity_regularizer;
            Parameters["kernel_constraint"] = kernel_constraint;
            Parameters["bias_constraint"] = bias_constraint;
        }

    }
}
