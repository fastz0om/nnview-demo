﻿using System;
using System.Collections.Generic;
using System.Text;
using Keras.Callbacks;
using Keras.Layers;
using Keras.Models;

namespace NNmodule.models
{
    class ClassifierNeuroNet : NeuroNet, IClassifier
    {
        ClassifierNeuroNet(string name, string description, List<Dictionary<string, object>> layersDescription,
            List<BaseLayer> layers = null, bool isTrained = false, Sequential model = null)
            : base(name, description, layersDescription, layers, isTrained, model)
        {

        }

        public History Teach(Teacher teacher, int epochQuantity, bool initWeights, List<List<Image>> trainingSamples, List<List<Image>> validateSamples)
        {
            base.Compile();
            _sequential.Compile(teacher.optimizer, teacher.loss, teacher.metrics, teacher.loss_weights,
                teacher.sample_weight_mode, teacher.weighted_metrics);
            return null;// _sequential.Fit(); 
        }


    }
}
