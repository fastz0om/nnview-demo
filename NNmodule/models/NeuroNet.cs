﻿using System;
using System.Collections.Generic;
using System.Text;
using Keras.Layers;
using Keras.Models;

namespace NNmodule.models
{
    abstract class NeuroNet : INeuroNet
    {
        protected String _name;
        protected String _description;
        protected List<Dictionary<string, object>> _layersDescription;
        protected List<BaseLayer> _layers;
        protected bool _isTrained = false;
        protected Sequential _sequential;

        public NeuroNet(String name, String description,
            List<Dictionary<string, object>> layersDescription,
            List<BaseLayer> layers = null, bool isTrained = false, Sequential model = null)
        {

        }

        public Sequential Model { get => _sequential; set => _sequential = value; }
        public String Name { get => _name; set => _name = value; }
        public String Description { get => _description; set => _description = value; }
        public bool IsTrained { get => _isTrained; set => _isTrained = value; }
        public List<BaseLayer> Layers { get => _layers; set => _layers = value; }
        public List<Dictionary<string, object>> LayersDescription { get => _layersDescription; set => _layersDescription = value; }

        public void AddLayer(Dictionary<string, object> layer)
        {
            if (false)
            {
                throw new Exception("Невозможно добавить слой, указана неправильная размерность");
                //TODO Add some logic here 
            }
            this._layersDescription.Add(layer);
        }
        public void DeleteLayer(int layerIndex)
        {
            this.LayersDescription.RemoveAt(layerIndex);
        }

        public void Compile()
        {
            if (this._sequential == null)
            {
                this._sequential = new Sequential();
            }
            else
            {
                this._sequential.Dispose();
                this._sequential = new Sequential();
            }
            foreach (Dictionary<string, object> obj in this._layersDescription)
            {
                this._sequential.Add(Utils.DescriptionToBaseLayer(obj));
            }
            // TODO add Teather to model or interface
            //this._sequential.Compile()
        }


        public String GetActivationType(int layerIndex)
        {
            return (String)this._layersDescription[layerIndex]["activation"];
        }


        public int GetLayersNumber()
        {
            return this._layersDescription.Count;
        }

        public String GetLayerType(int layerIndex)
        {
            return (String)this._layersDescription[layerIndex]["type"];
        }

        public int GetNeuronsNumber()
        {
            // TODO how we be able to count this? 
            throw new NotImplementedException();
        }

        public void Save(String path)
        {
            this._sequential.Save(path + ".nn");
            // TODO implements serializations for save method

            throw new NotImplementedException();
        }
    }
}
