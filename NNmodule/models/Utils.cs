﻿using System;
using System.Collections.Generic;
using System.Text;
using Keras.Layers;
using Numpy;

namespace NNmodule.models
{
    public static class Utils
    {
        public static BaseLayer DescriptionToBaseLayer(Dictionary<string, object> description)
        {
            static Keras.Shape GetShape2D(Tuple<int, int> tuple)
            {
                if (tuple != null)
                {
                    int[] mas = new int[2];
                    mas[0] = tuple.Item1;
                    mas[1] = tuple.Item2;
                    return new Keras.Shape(mas);
                }
                return null;
            }

            if (description["type"].Equals("FulCon"))
            {
                return new Dense((int)description["filters"], null, (string)description["activation"],
                    (bool)description["use_bias"], (string)description["kernel_initializer"],
                    (string)description["bias_initializer"], (string)description["kernel_regularizer"],
                    (string)description["bias_regularizer"], (string)description["activity_regularizer"],
                    (string)description["kernel_constraint"], (string)description["bias_constraint"],
                    GetShape2D((Tuple<int, int>)description["input_shape"]));
            }
            throw new NotImplementedException("Layer type not implemented");
        }

        public static List<NDarray> ImageList2DToNDarray(List<List<Image>> images)
        {
            // Нужно сделать из List<List<Image>> сначала обычные массивы C#(я хз какие массивы нужны так как не понимаю что такое Image)
            //, а потом каким то образом перекодировать в NumPy массивы NDarray
            // Причём должно получиться 2 массива data и labels . 
            //data хз скольки мерный массив так опять таки я не понимаю Image, labels - одномерный
            NDarray data;
            NDarray labels;
            throw new NotImplementedException();
        }


    }
}
