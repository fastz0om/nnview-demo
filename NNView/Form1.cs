﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NNView
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            TreeNode testTreeNode = new TreeNode();
            testTreeNode.Text = "Дааа, ну и дела";
            testTreeNode.ImageIndex = 0;
            testTreeNode.Tag = "qqq";
            treeView1.Nodes.Add(testTreeNode);
        }

        private int indexOfItemUnderMouseToDrag;

        private Point point12 = new Point(0, 0);
        private Rectangle dragBoxFromMouseDown;
        private Point screenOffset;

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            indexOfItemUnderMouseToDrag = treeView1.SelectedNode.Index;
        }


        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        private void tabPage1_Click_1(object sender, EventArgs e)
        {

        }

        private void идентификацияToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void treeView2_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }



        private void treeview1_MouseDown(object sender, MouseEventArgs e)
        {

            if (indexOfItemUnderMouseToDrag != ListBox.NoMatches)
            {

                // DragSize  показывает на сколько можно сместить мышку, чтоб произошло событие
                Size dragSize = SystemInformation.DragSize;

                // Создаем прямоугольник в центре которого расположен курсор
                dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                                                               e.Y - (dragSize.Height / 2)), dragSize);
            }
            else
                // Сбрасываем наш прямоугольник если мышка не на каком-либо элементе в ListView.
                dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void treeView1_MouseUp(object sender, MouseEventArgs e)
        {
            // Сбросить прямоугольник если кнопка отпущена
            dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void treeView1_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                // Если курсор вышел за пределы ListView - начинаем перетаскивание
                if (dragBoxFromMouseDown != Rectangle.Empty &&
                    !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    // screenOffset служить для определения границ экрана
                    screenOffset = SystemInformation.WorkingArea.Location;

                    DragDropEffects dropEffect = treeView1.DoDragDrop(treeView1.SelectedNode.Index, DragDropEffects.All);

                    // Если было выбрано "перемещение" удалить элемент из ListView
                    if (dropEffect == DragDropEffects.Move)
                    {
                        //Также можете ничего не удалять или уменьшать количество(изменять какие-то параметры)
                        treeView1.SelectedNode.Remove();
                    }
                }
            }
        }

        //private void listBox1_DragOver(object sender, DragEventArgs e)
        //{
        //    if ((e.KeyState & 8) == 8 &&
        //        (e.AllowedEffect & DragDropEffects.Copy) == DragDropEffects.Copy)
        //    {
        //        // Ctrl для копирования
        //        e.Effect = DragDropEffects.Copy;
        //    }
        //    else if ((e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move)
        //    {
        //        // По умолчанию перемещение
        //        e.Effect = DragDropEffects.Move;
        //    }
        //    else
        //        e.Effect = DragDropEffects.None;
        //    // Можно добавить другие кнопки или действия при перетаскивании

        //    // Получаем индекс над которым расположен курсор
        //    // Так как позиция мышки вычисляется по в координатах всего экрана конвертируем 
        //    // координаты относительно рабочего окна
        //    indexOfItemUnderMouseToDrop =
        //        listBox1.IndexFromPoint(listBox1.PointToClient(new Point(e.X, e.Y)));

        //    // Изменение текста нашего вспомогательного Label
        //    if (indexOfItemUnderMouseToDrop != ListBox.NoMatches)
        //        label1.Text = "Вставить над элементом #" + (indexOfItemUnderMouseToDrop + 1);
        //    else
        //        label1.Text = "Вставить в конец";
        //}

        //private void listBox1_DragDrop(object sender, DragEventArgs e)
        //{
        //    // Проверим, что перетаскиваемый не пустой
        //    Object item = (object)e.Data.GetData(typeof(LaeyrDescription));

        //    // Выбираем действием в зависимости от действия(перемещения или копирования)
        //    if (e.Effect == DragDropEffects.Copy ||
        //        e.Effect == DragDropEffects.Move)
        //    {

        //        //    // Вставка элемента в новый список
        //        //    if (indexOfItemUnderMouseToDrop != ListBox.NoMatches)
        //        //        listBox1.Items.Insert(indexOfItemUnderMouseToDrop, ((ListViewItem)item).Tag.ToString());
        //        //    else
        //        //        listBox1.Items.Add(((ListViewItem)item).Tag.ToString());
        //        //}
        //        // Вставка элемента в новый список
        //        if (indexOfItemUnderMouseToDrop != ListBox.NoMatches)
        //            listBox1.Items.Insert(indexOfItemUnderMouseToDrop, ((LaeyrDescription)item).tempStr);
        //        else
        //            listBox1.Items.Add(((LaeyrDescription)item).tempStr);
        //    }
        //    // Так как ничего не перетаскивается, устанавливаем значение "None"
        //    label1.Text = "None";
        //}

        //private void listBox1_DragEnter(object sender, DragEventArgs e)
        //{
        //    // Устанавливаем значение "None"
        //    label1.Text = "None";
        //}

        //private void listBox1_DragLeave(object sender, EventArgs e)
        //{
        //    // Устанавливаем значение "None", когда курсор вышел за пределы объекта, в который перетаскиваем элемент
        //    label1.Text = "None";
        //}


        private void panel1_DragOver(object sender, DragEventArgs e)
        {
            if ((e.KeyState & 8) == 8 &&
                (e.AllowedEffect & DragDropEffects.Copy) == DragDropEffects.Copy)
            {
                // Ctrl для копирования
                e.Effect = DragDropEffects.Copy;
            }
            else if ((e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move)
            {
                // По умолчанию перемещение
                e.Effect = DragDropEffects.Move;
            }
            else
                e.Effect = DragDropEffects.None;
            // Можно добавить другие кнопки или действия при перетаскивании

        }

        private void panel1_DragDrop(object sender, DragEventArgs e)
        {
            // Проверим, что перетаскиваемый не пустой
            Object item = (object)e.Data.GetData(typeof(MyTreeNode));

            // Выбираем действием в зависимости от действия(перемещения или копирования)
            if (e.Effect == DragDropEffects.Copy ||
                e.Effect == DragDropEffects.Move)
            {
                MyPictureBox myPict = new MyPictureBox();
                myPict.Image = imageList1.Images[0];
                point12.X += 50;
                point12.Y += 50;
                myPict.Location = point12;

                panel1.Controls.Add(myPict);

            }
        }

        private void panel1_DragEnter(object sender, DragEventArgs e)
        {

        }

        private void panel1_DragLeave(object sender, EventArgs e)
        {

        }

    }

    //public class LaeyrDescription : ListViewItem
    //{
    //    public Dictionary<string, object> _descripDic = new Dictionary<string, object>();
    //    public string tempStr = "KZI";

    //    public LaeyrDescription()
    //    {
    //    }

    //    public EventHandler e;

    //}

    public class MyTreeNode : TreeNode
    {
        public Dictionary<string, object> _descripDic = new Dictionary<string, object>();
        public string tempStr = "KZI";

        public MyTreeNode()
        {
        }

        public EventHandler e;

    }



    public class MyPictureBox : PictureBox
    {


        public MyPictureBox()
        {
            base.MouseDoubleClick += new MouseEventHandler(pict_DoubleClick);
        }
        public void pict_DoubleClick(object sender, EventArgs e)
        {
            //Открыть новую форму
        }
    }
}
