﻿using SHV.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace NNView
{
    public class Integraction
    {
        static private MainWindowConstructor _form;
        static public void LoadNNModule(Form owner, Options options)
        {
            if (_form == null)
                _form = new MainWindowConstructor();
            owner.Hide();
            _form.ShowDialog();
            owner.Show();
        }
    }
}
