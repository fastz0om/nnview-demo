﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using NNView.controls;
using PrototypeSystem;
using PrototypeSystem.NNmodule.models;
using PrototypeSystem.NNmodule.layers;
using System.Drawing;

namespace NNView.controls
{
    public class MyPanel : Panel
    {
        public MyPanel(ContextMenuStrip contextMenuStrip, ListView listView, ImageList imageList)
        {
            this.AllowDrop = true;
            this.AutoScroll = true;
            this.Dock = DockStyle.Fill;
            this.Location = new Point(0, 0);
            this.Size = new Size(522, 489);
            this.TabIndex = 0;
            this.DragDrop += new DragEventHandler(this.Panel1_DragDrop);
            this.DragOver += new DragEventHandler(this.Panel1_DragOver);
            this.Paint += new PaintEventHandler(this.Panel1_Paint);
            this.MouseClick += new MouseEventHandler(this.Panel1_MouseClick);
            this.MouseMove += new MouseEventHandler(this.Panel1_MouseMove);
            this.MouseDoubleClick += new MouseEventHandler(this.Panel1_MouseDoubleClick);

            _selectedFigures.Add(new List<Line>());
            _selectedFigures.Add(new List<Line>());

            this.contextMenuStripInOut = contextMenuStrip;
            this.listViewLayers = listView;
        }

        //Индекс элемента, который перетягиваем на панель
        public int _indexOfItemUnderMouseToDrag;
        //Позиция пикчер бокса, который создаем
        private Point _pictureBoxLocation = new Point(0, 0);
        //Лист пикчербоксов, которые уже созданы
        public List<MyPictureBox> _listPictureBox = new List<MyPictureBox>();
        // Набор фигур
        public FiguresCollection _figures = new FiguresCollection();
        // Главная нейросеть
        public PrototypeModel _mainNeuroNet;
        //Временный PictureBox, нужен для обработки событий PictureBox'ов
        private MyPictureBox _tempPictureBox;
        //Листы линий, которым нужно сменить начальную или конечную координату (в зависимостио от номера листа в который они попали)
        private List<List<Line>> _selectedFigures = new List<List<Line>>();
        //Была ли создана нейросеть?
        public bool isNeuroNetCreate = false;
        //Идет перенос линии или нет?
        public bool doDraw = false;
        //Контекст меню стрим при нажатии на пикчербоксы
        private ContextMenuStrip contextMenuStripInOut;
        //Лист слоев, которые доступны для перемещения
        private ListView listViewLayers;
        //Лист изображений для того, чтобы для каждого слоя создавалось определённое изображение
        //   private ImageList largeImageList;
        //Лист лейблов, хранящих некоторые параметры слоёв
        public List<Label> labels = new List<Label>();

        //Обработчик DragOver на графической панели
        private void Panel1_DragOver(object sender, DragEventArgs e)
        {
            if ((e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move)
            {
                e.Effect = DragDropEffects.Move;
            }
            else
                e.Effect = DragDropEffects.None;
        }

        //Обработчик DragDrop на графической панели
        private void Panel1_DragDrop(object sender, DragEventArgs e)
        {
            // Проверим, что перетаскиваемый не пустой
            MyListViewItem item1 = (MyListViewItem)e.Data.GetData(typeof(MyListViewItem));
            // Выбираем действием в зависимости от действия(перемещения или копирования)
            if (e.Effect == DragDropEffects.Move)
            {
                CreatePictureBoxFor(listViewLayers.Items[_indexOfItemUnderMouseToDrag].Text);
            }
        }

        //Создает пикчербокс, когда необходимо создать или подгрузить слой или подсеть
        public void CreatePictureBoxFor(string layerName, Prototype prototype = null, bool isNeuroNet = false)
        {
            MyPictureBox myPict = null;
            if (prototype == null)
            {
                myPict = new MyPictureBox(PrototypeSystem.utils.Factories.CreatingLayerInstance(layerName));
            }
            else myPict = new MyPictureBox(prototype);

            myPict.ContextMenuStrip = this.contextMenuStripInOut;
            myPict.Image = new Bitmap(UtilsUI.GetLayerImage(layerName), UtilsUI.GetLayerImageSize(layerName));
            _pictureBoxLocation.X += 50;
            _pictureBoxLocation.Y += 50;
            myPict.Location = _pictureBoxLocation;
            myPict.MouseDown += new MouseEventHandler(PictureBox_MouseDown);
            myPict.MouseUp += new MouseEventHandler(PictureBox_MouseUp);
            myPict.MouseMove += new MouseEventHandler(PictureBox_MouseMove);
            Controls.Add(myPict);
            _listPictureBox.Add(myPict);

            if (isNeuroNet)
            {
                Label label = new Label();
                label.Text = prototype.Name;
                label.AutoSize = true;
                label.Location = new Point(myPict.Location.X - 10, myPict.Location.Y + myPict.Size.Height);
                Controls.Add(label);
                labels.Add(label);
            }
            else myPict.MouseDoubleClick += new MouseEventHandler(PictureBox_DoubleClick);

        }

        //Обработчик Paint на графической панели
        private void Panel1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            _figures.Draw(e.Graphics);
        }

        //Обработчик движения мыши на графической панели
        private void Panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (doDraw && _figures != null && _figures.Count != 0)
            {
                _figures[_figures.Count - 1].EndPoint = new PointF(e.X, e.Y);
                Invalidate();
            }
        }

        //Обработчик нажатия мыши на графической панели
        private void Panel1_MouseClick(object sender, MouseEventArgs e)
        {
            if (MouseButtons.Right == e.Button && doDraw && _figures != null && _figures.Count != 0)
            {
                doDraw = false;
                contextMenuStripInOut.Items[0].Enabled = false;
                contextMenuStripInOut.Items[1].Enabled = true;
                _figures.Remove(_figures[_figures.Count - 1]);
                Invalidate();
            }
        }

        //Обработчик двойного нажатия клавиши мыши и удаления связи между слоями
        private void Panel1_MouseDoubleClick(object sender, MouseEventArgs e)
        {

            Line selectedFigure = (Line)_figures.GetFigure(e.X, e.Y);
            if (selectedFigure != null)
            {
                bool result = UtilsUI.CreateMessageWindow("Удалить связь между слоями?");
                if (result)
                {
                    if (_mainNeuroNet.Level == 1)
                    {
                        _listPictureBox[selectedFigure.indexStartPictOut]._prototype.DeleteOutput(_listPictureBox[selectedFigure.indexEndPictIn]._prototype);
                        _listPictureBox[selectedFigure.indexEndPictIn]._prototype.DeleteInput(_listPictureBox[selectedFigure.indexStartPictOut]._prototype);
                    }
                    else if (_mainNeuroNet.Level == 2)
                    {
                        ((PrototypeModel)_listPictureBox[selectedFigure.indexEndPictIn]._prototype).DeleteInputModel((PrototypeModel)_listPictureBox[selectedFigure.indexStartPictOut]._prototype);
                        ((PrototypeModel)_listPictureBox[selectedFigure.indexStartPictOut]._prototype).DeleteOutputModel((PrototypeModel)_listPictureBox[selectedFigure.indexEndPictIn]._prototype);
                    }
                    _figures.Remove(selectedFigure);
                    Invalidate();
                }
            }
        }

        //Обработчик нажатия мыши на PictureBox
        private void PictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _tempPictureBox = sender as MyPictureBox;
                _selectedFigures = UtilsUI.SearchLinesConnectedPictrureBox(_listPictureBox.IndexOf(_tempPictureBox), _figures);
            }
        }

        //Обработчик отпуска мыши на PictureBox
        private void PictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (_tempPictureBox != null && (_tempPictureBox.Left < 0 || _tempPictureBox.Top < 0))
            {
                _tempPictureBox.Location = new Point(0, 0);
                PictureBox_MouseMove(sender, e);
            }
            _selectedFigures[0].Clear();
            _selectedFigures[1].Clear();
        }

        //Обработчик движения мыши на PictureBox
        private void PictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _tempPictureBox.Left = e.X + _tempPictureBox.Left;
                _tempPictureBox.Top = e.Y + _tempPictureBox.Top;
                if (labels != null && labels.Count != 0)
                {
                    labels[_listPictureBox.IndexOf(_tempPictureBox)].Top += e.Y;
                    labels[_listPictureBox.IndexOf(_tempPictureBox)].Left += e.X;
                }
                if (_selectedFigures[0].Count != 0 || _selectedFigures[1].Count != 0)
                {
                    foreach (Line ss in _selectedFigures[0])
                    {
                        ss.StartPoint = new PointF(e.X + _tempPictureBox.Location.X + _tempPictureBox.Size.Width / 2,
                      e.Y + _tempPictureBox.Location.Y + _tempPictureBox.Size.Height / 2);
                    }
                    foreach (Line ss in _selectedFigures[1])
                    {
                        ss.EndPoint = new PointF(e.X + _tempPictureBox.Location.X + _tempPictureBox.Size.Width / 2,
                            e.Y + _tempPictureBox.Location.Y + _tempPictureBox.Size.Height / 2);
                    }
                }
                Invalidate();
            }
        }

        //Обработчик двойного нажатия мыши на пикчербокс
        public void PictureBox_DoubleClick(object sender, EventArgs e)
        {
            _ = new WindowParameterConfiguration((sender as MyPictureBox)._prototype, 1);
        }      
    }
}
